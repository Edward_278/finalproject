﻿using MyFinalPr.Core.Abstractions.Repositories;
using MyFinalPr.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinalPr.DAL.Repasitories
{
    public class CustomerRepository : SqlRepositoryBase<Customer>, ICustomerRepository
    {
        private readonly NORTHWNDContext _context;
        public CustomerRepository(NORTHWNDContext dbContext)
            : base(dbContext)
        {
            _context = dbContext;
        }


        public Array getCustomersByCountryAndCity()
        {
            var result = _context.Customers
                                 .GroupBy(x => new { x.Country, x.City })
                                 .Select(x => new { x.Key.Country, x.Key.City, CutomersCount = x.Count() })
                                 .OrderByDescending(x => x.CutomersCount)
                                 .ToArray();
            return result;
        }

        public Array CustomerByRegion()
        {
            var temp = _context.Customers
                     .OrderBy(x => x.Region)
                     .ThenBy(x => x.CustomerId)
                     .Select(x => new { x.CompanyName, x.Region })
                     .ToList();
            var temp1 = temp.Where(x => x.Region != null).ToList();
            var temp2 = temp.Where(x => x.Region == null).ToList();
            foreach (var item in temp2)
            {
                temp1.Add(item);
            }
            var sortedCustumersByRegion = temp1.ToArray();
            return sortedCustumersByRegion;
        }

    }
}
