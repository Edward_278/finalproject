﻿using MyFinalPr.Core.Abstractions.Repositories;
using MyFinalPr.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinalPr.DAL.Repasitories
{
    public class OrderRepository : SqlRepositoryBase<Order>, IOrderRepository

    {
        private readonly NORTHWNDContext _context;
        public OrderRepository(NORTHWNDContext dbContext)
          : base(dbContext)
        {
            _context = dbContext;
        }

        public Array HighFreightCharges()
        {
            var result = _context.Orders.GroupBy(x => x.ShipCountry)
                                        .Select(x => new { x.Key, AverrageFreight = x.Average(x => x.Freight) })
                                        .OrderByDescending(x => x.AverrageFreight)
                                        .Take(3)
                                        .ToArray();
            return result;
        }

        public Array HighFreightChargesOfYear()
        {
            var result = _context.Orders.Where(x => x.OrderDate.Value.Year == 1997)
                                        .GroupBy(x => x.ShipCountry)
                                        .Select(x => new { x.Key, AverrageFreight = x.Average(x => x.Freight) })
                                        .OrderByDescending(x => x.AverrageFreight)
                                        .Take(3)
                                        .ToArray();
            return result;
        }

        public Array HighFreightChargesBetween()
        {

            var result = _context.Orders.Where(x => (x.OrderDate.Value.Year > 1996
                                                && x.OrderDate.Value.Year < 1997))
                                        .GroupBy(x => x.ShipCountry)
                                        .Select(x => new { x.Key, AverrageFreight = x.Average(x => x.Freight) })
                                        .OrderByDescending(x => x.AverrageFreight)
                                        .Take(3)
                                        .ToArray();
            return result;
        }

        public Array HighFreightChargesLastYear()
        {
            var result = _context.Orders.GroupBy(x => x.ShipCountry)
                                        .Select(x => new
                                        {
                                            x.Key,
                                            MaxFreight = x.Max(x => x.Freight),
                                            Max = x.Max(x => x.OrderDate.Value.Year)
                                        })
                                        .OrderByDescending(x => x.Max)
                                        .Take(3)
                                        .ToArray();
            return result;
        }
    }
}
