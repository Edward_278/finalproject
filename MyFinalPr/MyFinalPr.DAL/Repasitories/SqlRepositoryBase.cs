﻿using MyFinalPr.Core.Abstractions.Repositories;
using MyFinalPr.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinalPr.DAL.Repasitories
{
 
    public class SqlRepositoryBase<T> : ISqlRepository<T> where T:class
    {
        protected NORTHWNDContext Context { get; }

        protected SqlRepositoryBase(NORTHWNDContext northwindContext)
        {
            Context = northwindContext;
        }
        public T Add(T entity)
        {
            Context.Set<T>().Add(entity);
            return entity;
        }

        public void Edit(T entity)
        {

            Context.Set<T>().Update(entity);
        }

        public T Get(int id)
        {
            return Context.Set<T>().Find(id);

        }

        public IEnumerable<T> GetWhere(Func<T, bool> predicate)
        {
            return Context.Set<T>().Where<T>(predicate);
        }

        public void Remove(T entity)
        {
            Context.Set<T>().Remove(entity);
        }
    }
}
