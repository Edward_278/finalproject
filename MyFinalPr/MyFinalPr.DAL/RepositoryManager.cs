﻿using Microsoft.Data.SqlClient;
using MyFinalPr.Core.Abstractions;
using MyFinalPr.Core.Abstractions.Repositories;
using MyFinalPr.DAL.Repasitories;
using MyFinalPr.Models.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinalPr.DAL
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly NORTHWNDContext _dbContext;
        public RepositoryManager(NORTHWNDContext northwindContext)
        {
            _dbContext = northwindContext;
        }

        private IOrderRepository _orders;
        public IOrderRepository Orders => _orders ?? (_orders = new OrderRepository(_dbContext));
        private ICustomerRepository _customers;
        public ICustomerRepository Customers => _customers ?? (_customers = new CustomerRepository(_dbContext));
        private IProductRepository _products;
        public IProductRepository Products => _products ?? (_products = new ProductRepository(_dbContext));

        public ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted)
        {
            return SqlTransaction.Begin(_dbContext, isolation);
        }

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _dbContext.SaveChangesAsync();
        }


    }
}
