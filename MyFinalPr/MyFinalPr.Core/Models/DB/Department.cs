﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MyFinalPr.Models.DB
{
    public partial class Department
    {
        public int DId { get; set; }
        public string Name { get; set; }
    }
}
