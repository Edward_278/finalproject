﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MyFinalPr.Models.DB
{
    public partial class Newtable2021
    {
        public int AddressId { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
    }
}
