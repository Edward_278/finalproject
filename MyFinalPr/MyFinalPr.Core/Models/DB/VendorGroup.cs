﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MyFinalPr.Models.DB
{
    public partial class VendorGroup
    {
        public VendorGroup()
        {
            Vendors = new HashSet<Vendor>();
        }

        public int GroupId { get; set; }
        public string GroupName { get; set; }

        public virtual ICollection<Vendor> Vendors { get; set; }
    }
}
