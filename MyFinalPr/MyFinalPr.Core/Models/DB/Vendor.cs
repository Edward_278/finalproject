﻿using System;
using System.Collections.Generic;

#nullable disable

namespace MyFinalPr.Models.DB
{
    public partial class Vendor
    {
        public int VendorId { get; set; }
        public string VendorName { get; set; }
        public int GroupId { get; set; }

        public virtual VendorGroup Group { get; set; }
    }
}
