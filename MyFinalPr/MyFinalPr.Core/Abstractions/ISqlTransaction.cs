﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinalPr.Core.Abstractions
{
    public interface ISqlTransaction : IDisposable
    {
        void Commit();
        void Rollback();
    }
}
