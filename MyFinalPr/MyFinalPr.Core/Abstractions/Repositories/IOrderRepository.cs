﻿using MyFinalPr.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinalPr.Core.Abstractions.Repositories
{
    public interface IOrderRepository : ISqlRepository<Order>
    {
        Array HighFreightCharges();
        Array HighFreightChargesOfYear();
        Array HighFreightChargesBetween();
        Array HighFreightChargesLastYear();
    }
}
