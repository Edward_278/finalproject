﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinalPr.Core.Abstractions.Operations
{
    public interface IProductBL
    {
        Array SortProducts();
        Array ProductsNeedReordering();
        Array ProductNeedReorderingContinueted();
    }
}
