﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinalPr.Core.Abstractions.Operations
{
    public interface ICustomerBL
    {
        Array getCustomersByCountryAndCity();
        Array CustomerByRegion();
    }
}
