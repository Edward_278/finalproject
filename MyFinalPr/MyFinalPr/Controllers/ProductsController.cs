﻿using Microsoft.AspNetCore.Mvc;
using MyFinalPr.Core.Abstractions.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyFinalPr.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductBL _productBL;
        public ProductsController(IProductBL productBL)
        {
            _productBL = productBL;
        }
        [HttpGet("Sort")]
        public IActionResult Sort()
        {
            var result = _productBL.SortProducts();
            return Ok(result);
        }
        [HttpGet("Reordering")]
        public IActionResult NeedReordering()
        {
            var result = _productBL.ProductsNeedReordering();
            return Ok(result);
        }
        [HttpGet("Reordering/Discontinued")]
        public IActionResult NeedReorderingWith()
        {
            var result = _productBL.ProductNeedReorderingContinueted();
            return Ok(result);
        }
    }
}
