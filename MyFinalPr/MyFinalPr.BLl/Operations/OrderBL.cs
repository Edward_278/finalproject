﻿using MyFinalPr.Core.Abstractions;
using MyFinalPr.Core.Abstractions.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinalPr.BLL.Operations
{
    public class OrderBL : IOrderBL
    {

        private readonly IRepositoryManager _repositoryManager;
        public OrderBL(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }
        public Array HighFreightCharges()
        {

            return _repositoryManager.Orders.HighFreightCharges();
        }

        public Array HighFreightChargesBetween()
        {
            return _repositoryManager.Orders.HighFreightChargesBetween();
        }

        public Array HighFreightChargesLastYear()
        {
            throw new NotImplementedException();
        }

        public Array HighFreightChargesOfYear()
        {
            return _repositoryManager.Orders.HighFreightChargesOfYear();
        }

    }
}
