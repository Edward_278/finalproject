﻿using MyFinalPr.Core.Abstractions;
using MyFinalPr.Core.Abstractions.Operations;
using MyFinalPr.Core.Abstractions.Repositories;
using MyFinalPr.DAL.Repasitories;
using MyFinalPr.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinalPr.BLL.Operations
{
    public class ProductBL : IProductBL
    {
        private readonly IRepositoryManager _repositoryManager;
        public ProductBL(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }
        public Array SortProducts()
        {
            var SortedProducts = _repositoryManager.Products.SortProducts();
            return SortedProducts;
        }
        public Array ProductsNeedReordering()
        {
            return _repositoryManager.Products.ProductsNeedReordering();
        }
        public Array ProductNeedReorderingContinueted()
        {
            return _repositoryManager.Products.ProductNeedReorderingContinueted();
        }

    }
}
