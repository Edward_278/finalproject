﻿using MyFinalPr.Core.Abstractions;
using MyFinalPr.Core.Abstractions.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinalPr.BLL.Operations
{
    public class CustomerBL : ICustomerBL
    {

        private readonly IRepositoryManager _repositoryManager;
        public CustomerBL(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }


        public Array getCustomersByCountryAndCity()
        {
            var GetCustomer = _repositoryManager.Customers.getCustomersByCountryAndCity();
            return GetCustomer;


        }

        public Array CustomerByRegion()
        {
            return _repositoryManager.Customers.CustomerByRegion();
        }
    }
}
